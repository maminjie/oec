#!/bin/bash
#Config environment before develop, please run: source ./develop_env.sh

codiag_path=$(cd $(dirname ${BASH_SOURCE}); pwd)
python_paths=$(echo ${PYTHONPATH} | sed 's/:/ /g')
existed=0
for path in $python_paths; do
	if [ "$codiag_path" = "$path" ]; then
		existed=1
	fi
done

if [ $existed -eq 0 ]; then
	export PYTHONPATH=${PYTHONPATH}:${codiag_path}
fi
echo "PYTHONPATH=${PYTHONPATH}"

