#!/usr/bin/env python3
# Copyright (c) 2020-2021 maminjie <maminjie1@huawei.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest
import json
import time
import re
import sys
import os
import yaml

base_path = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/..")
if base_path not in sys.path:
    sys.path.append(base_path)

from oec.libs import openeuler

class TestOpenEuler(unittest.TestCase):
    """
    Test OpenEuler
    """
    def setUp(self):
        self.oe = openeuler.OpenEuler()

    def tearDown(self):
        pass

    def test_get_contributors(self):
        contributors = self.oe.get_contributors("src-openeuler", "libffado")
        print(contributors)

    def test_get_yaml(self):
        self.oe.get_yaml("oec")
        pkg_info = self.oe.get_yaml("osc")
        pkg_yaml = yaml.load(pkg_info, Loader=yaml.Loader)
        self.assertEqual("openSUSE/osc", pkg_yaml.get("src_repo"))
        self.oe.get_yaml("osc", "openEuler-20.03")

    def test_get_spec(self):
        print(self.oe.get_spec("oec"))
        spec_info = self.oe.get_spec("osc")
        self.assertIsNotNone(spec_info)

    def test_get_sigs(self):
        sigs = self.oe.get_sigs()
        self.assertIsNotNone(sigs)

    def test_fork_repo(self):
        self.oe.fork_repo("openeuler", "maminjie", "pkgship")
        self.oe.delete_repo("maminjie", "pkgship")


if __name__ == "__main__":
    unittest.main()

