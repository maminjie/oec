#!/usr/bin/env python3
# Copyright (c) 2020-2021 maminjie <maminjie1@huawei.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest
import json
import time
import re
import sys
import os

base_path = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/..")
if base_path not in sys.path:
    sys.path.append(base_path)

from oec.libs import codestat

class TestCodeStat(unittest.TestCase):
    """
    Test code statistics tool
    """
    def setUp(self):
        self.code_stat = codestat.CodeStat()

    def tearDown(self):
        pass

    def test_get_contributors(self):
        contributors = self.code_stat.get_contributors("src-openeuler", "libffado")
        print(contributors)

    def test_get_branches(self):
        branches = self.code_stat.get_branches("src-openeuler", "libffado")
        print(branches)

    def test_get_reviewers(self):
        reviewers = self.code_stat.get_reviewers("src-openeuler", "libffado")
        print(reviewers)

    def test_get_commits(self):
        commits = self.code_stat.get_commits("src-openeuler", "libffado")
        print(commits)

    def test_get_commits_1(self):
        commits = self.code_stat.get_commits("src-openeuler", "mysql", sha="master", author="maminjie")
        print(commits)

    def test_get_commits_2(self):
        commits = self.code_stat.get_commits("src-openeuler", "log4j", sha="master", author="wang_yue111")
        print(commits)

    def test_get_one_commit(self):
        commits = self.code_stat.get_one_commit("src-openeuler", "mysql", "master")
        print(commits)

    def test_get_one_commit_1(self):
        commit = self.code_stat.get_one_commit("src-openeuler", "mysql", "master") # last one commit
        print(commit)

    def test_get_one_commit_2(self):
        commit = self.code_stat.get_one_commit("src-openeuler", "mysql", "570192bd22987fe4df428d5414648d9b85f285a8")
        print(commit)

    def test_get_pr_list(self):
        prs = self.code_stat.get_pr_list("src-openeuler", "libffado", head="maminjie:master", base="master")
        print(prs)

    def test_get_pr_comments(self):
        prs = self.code_stat.get_pr_comments("src-openeuler", "libffado", 4)
        print(prs)

    def test_get_org_repos(self):
        repos = self.code_stat.get_org_repos("src-openeuler")
        print(len(repos))

    def test_get_org_repos_all(self):
        page = 1
        per_page = 100
        repos = self.code_stat.get_org_repos("src-openeuler", page=page, per_page=per_page)
        while len(repos):
            # for repo in repos:
            #     print(repo)
            page += 1
            repos = self.code_stat.get_org_repos("src-openeuler", page=page, per_page=per_page)

    def test_merge_statistics(self):
        stats = self.code_stat.merge_statistics("maminjie", "src-openeuler", "libffado",
            start_time="2020-10-01", end_time="2020-10-30")
        print(stats)

    def test_merge_statistics_1(self):
        users = ["maminjie", "wang_yue111"]
        repos = ["efl", "libffado", "pytest", "log4j"]

        print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format("user", "repo", "branch", "state",
            "prNo.", "prType", "prTitle", "prAuthor", "failCnt", "rejectCnt",
            "addition", "deletion", "total", "reviewCnt", "reviewer", "time"))
        for user in users:
            for repo in repos:
                stats = self.code_stat.merge_statistics(user, "src-openeuler", repo)
                if not stats:
                    continue
                for stat in stats:
                    print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t+{}\t-{}\t{}\t{}\t{}\t{}".format(
                        stat["user"],
                        stat["repo"],
                        stat["branch"],
                        stat["state"],
                        stat["prNum"],
                        stat["prType"],
                        stat["prTitle"],
                        stat["prAuthor"],
                        stat["failCnt"],
                        stat["rejectCnt"],
                        stat["addition"],
                        stat["deletion"],
                        stat["total"],
                        stat["reviewCnt"],
                        stat["reviewer"],
                        stat["time"]))

    def test_contribute_statistics(self):
        stat = self.code_stat.contribute_statistics("maminjie", "src-openeuler", "libffado",
            start_time="2020-10-01", end_time="2020-10-30")
        print(stat)

    def test_contribute_statistics_1(self):
        users = ["maminjie", "wang_yue111"]
        repos = ["efl", "libffado", "pytest", "log4j"]

        print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(
            "user", "commitCnt", "mergeCnt", "rejectCnt", "failCnt", "addition", "deletion", "total", "reviewCnt"))
        for user in users:
            commitCnt = 0
            mergeCnt = 0
            rejectCnt = 0
            failCnt = 0
            addition = 0
            deletion = 0
            total = 0
            reviewCnt = 0
            for repo in repos:
                stat = self.code_stat.contribute_statistics(user, "src-openeuler", repo)
                if not stat:
                    continue
                commitCnt += stat["commitCnt"]
                mergeCnt += stat["mergeCnt"]
                rejectCnt += stat["rejectCnt"]
                failCnt += stat["failCnt"]
                addition += stat["addition"]
                deletion += stat["deletion"]
                total += stat["total"]
                reviewCnt += stat["reviewCnt"]
            print("{}\t{}\t{}\t{}\t{}\t+{}\t-{}\t{}\t{}".format(user, commitCnt, mergeCnt, rejectCnt,
                failCnt, addition, deletion, total, reviewCnt))

    def test_time_compare(self):
        s1 = "2020-09-19T09:10:37+08:00"
        s2 = "2020-09-20T09:10:37+08:00"
        if s1 < s2:
            print("yes")


if __name__ == "__main__":
    unittest.main()
