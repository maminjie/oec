#!/usr/bin/env bash
# Copyright (c) 2021 maminjie <maminjie1@huawei.com>
# SPDX-License-Identifier: MulanPSL-2.0

main() {
    local python_cmd=""
    
    type python3 > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        python_cmd=python3
    else
        type python > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            python_cmd=python
        fi
    fi
    
    if [ -n "$python_cmd" ]; then
        $python_cmd oec/core.py $@
    fi
}

main "$@"

