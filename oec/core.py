#!/usr/bin/env python3
# Copyright (c) 2020-2021 maminjie <maminjie1@huawei.com>
# SPDX-License-Identifier: MulanPSL-2.0

from __future__ import print_function

import sys
import os

base_path = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/..")
if base_path not in sys.path:
    sys.path.append(base_path)

from oec.user import *
from oec.utils import cmdln


__version__ = '0.0.1'


class Oec(cmdln.Cmdln):
    """Usage: oec [GLOBALOPTS] SUBCOMMAND [OPTS] [ARGS...]
    or: oec help SUBCOMMAND

    openEuler command-line tool.
    Type 'oec help <subcommand>' for help on a specific subcommand.

    ${command_list}
    ${help_list}
    global ${option_list}
    """

    name = 'oec'
    conf = None

    def __init__(self, *args, **kwargs):
        cmdln.Cmdln.__init__(self, *args, **kwargs)
        cmdln.Cmdln.do_help.aliases.append('h')

    def get_version(self):
        global __version__
        return __version__

    def get_optparser(self):
        """this is the parser for "global" options (not specific to subcommand)"""
        optparser = cmdln.CmdlnOptionParser(self, formatter_class=cmdln.CmdlnFormatter)
        optparser.add_argument('-v', '--version', action='version', version=__version__)
        return optparser

    def get_cmd_help(self, cmdname):
        doc = self._get_cmd_handler(cmdname).__doc__
        doc = self._help_reindent(doc)
        doc = self._help_preprocess(doc, cmdname)
        doc = doc.rstrip() + '\n' # trim down trailing space
        return self._str(doc)

    @cmdln.alias("cst")
    @cmdln.option("-o", "--outfile", type=str, default="",
                        help="The output file, default is {command}_stat_result.csv")
    @cmdln.option("-e", "--endtime", type=str, default="",
                        help="The end time, format is ISO 8601, default is not set")
    @cmdln.option("-s", "--starttime", type=str, default="",
                        help="The start time, format is ISO 8601, default is not set")
    @cmdln.option("-p", "--project", type=str, default="src-openeuler",
                        help="The project name, default is src-openeuler")
    @cmdln.option("-b", "--branch", type=str, default="all",
                        help="The branch name, default is all branch")
    @cmdln.option("-t", "--repotype", type=str, default="all",
                        help="The repository type, as follows: all/public/private, default is all")
    @cmdln.option("-r", "--repo", type=str, default="",
                        help="The repository file or list, default is all repos")
    @cmdln.option("-u", "--user", type=str,
                        help="The username file or list")
    @cmdln.option("-c", "--command", type=str, choices=['merge', 'contribute'], required=True,
                        help="The subcommand, as follows: merge, contribute")
    def do_codestat(self, subcmd, opts, *args):
        """${cmd_name}: Statistic the amount of code submitted to the gitee

        ${cmd_usage}
        ${cmd_option_list}
        """
        oe_codestat.do_main(opts)


if __name__ == "__main__":
    oec = Oec()
    sys.exit(oec.main())
