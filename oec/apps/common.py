#!/usr/bin/env python3
# Copyright (c) 2020-2021 maminjie <maminjie1@huawei.com>
# SPDX-License-Identifier: MulanPSL-2.0

import os

def get_list(arg):
    """
    convert the arg to list, the arg is a string or file
    the content of string or file is separated by spaces

    eg:
        arg = " a b  c d  "
        get_list(arg)    => ['a','b','c','d']
    """
    lst = []
    if os.path.isfile(arg):
        with open(arg, 'r') as f:
            for ln in f:
                lst.extend(ln.split())
    else:
        lst = list(arg.split())
    return lst
